﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterDeevakonda.Models;

namespace ConverterDeevakonda.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Sharanya";
            ViewData["Result"] = "";

            Converter converter = new Converter();
            return View(converter);
        }


       
        public IActionResult Convert(Converter converter)
        {
            double result;
            if (ModelState.IsValid)
            {

                result = (converter.Temperature - 32) * 5.0 / 9.0;
                ViewData["Title"] = "Converted by Sharanya";
                ViewData["Result"] = "Temperature in C = " +
                    (int)(result);
            }
            else if(!ModelState.IsValid) {

                ViewData["Title"] = "Please provide appropriate inputs";
            }

            return View("Index", converter);
        }

             
    }
}
